Source: dlib
Maintainer: Debian Science Maintainers <debian-science-maintainers@lists.alioth.debian.org>
Uploaders: Séverin Lemaignan <severin@guakamole.org>,
           Pierre Gruet <pgt@debian.org>
Section: libs
Priority: optional
Build-Depends: dpkg-dev (>= 1.22.5), bzip2,
               cmake,
               debhelper-compat (= 13),
               libblas-dev,
               libjpeg-dev,
               liblapack-dev,
               libpng-dev,
               libsqlite3-dev
Standards-Version: 4.7.0
Vcs-Browser: https://salsa.debian.org/science-team/dlib
Vcs-Git: https://salsa.debian.org/science-team/dlib.git
Homepage: http://dlib.net/
Rules-Requires-Root: no

Package: libdlib-dev
Architecture: any
Multi-Arch: same
Section: libdevel
Depends: libdlib19.1t64 (= ${binary:Version}),
         ${misc:Depends},
         libopencv-core-dev,
         libpng-dev,
         libsqlite3-dev,
         libx11-dev,
         pybind11-dev
Description: C++ toolkit for machine learning and computer vision - development
 Dlib is a general purpose cross-platform open source software library written
 in the C++ programming language. It now contains software components for
 dealing with networking, threads, graphical interfaces, complex data
 structures, linear algebra, statistical machine learning, image processing,
 data mining, XML and text parsing, numerical optimization, Bayesian networks,
 and numerous other tasks.
 .
 This package contains the development headers.

Package: libdlib19.1t64
Provides: ${t64:Provides}
Replaces: libdlib19.1
Breaks: libdlib19.1 (<< ${source:Version})
Architecture: any
Multi-Arch: same
Depends: libdlib-data,
         ${misc:Depends},
         ${shlibs:Depends}
Pre-Depends: ${misc:Pre-Depends}
Description: C++ toolkit for machine learning and computer vision - library
 Dlib is a general purpose cross-platform open source software library written
 in the C++ programming language. It now contains software components for
 dealing with networking, threads, graphical interfaces, complex data
 structures, linear algebra, statistical machine learning, image processing,
 data mining, XML and text parsing, numerical optimization, Bayesian networks,
 and numerous other tasks.

Package: libdlib-data
Architecture: all
Multi-Arch: foreign
Depends: ${misc:Depends}
Description: C++ toolkit for machine learning and computer vision - models
 Dlib is a general purpose cross-platform open source software library written
 in the C++ programming language. It now contains software components for
 dealing with networking, threads, graphical interfaces, complex data
 structures, linear algebra, statistical machine learning, image processing,
 data mining, XML and text parsing, numerical optimization, Bayesian networks,
 and numerous other tasks.
 .
 This package contains the trained models provided with dlib.
